package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TFloorGoods;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITFloorGoodsService extends IService<TFloorGoods> {
	
}
