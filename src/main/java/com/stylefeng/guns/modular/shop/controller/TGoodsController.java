/** Powered By 北京zscat科技, Since 2016 - 2020**/
package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.shiro.ShiroKit;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.persistence.shop.model.TGoods;
import com.stylefeng.guns.modular.shop.service.ITGoodsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 商品控制器
 * [email]
 * @author zscat 951449465
 * @Date 2017-5-25 14:16:26
 */
@Controller
@RequestMapping("/tGoods")
public class TGoodsController extends BaseController {

    private String PREFIX = "/shop/tGoods/";

    @Resource
    private   ITGoodsService TGoodsService;

    /**
     * 跳转到商品列表首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tGoods.html";
    }

    /**
     * 跳转到添加商品
     */
    @RequestMapping("/tGoods_add")
    public String tGoodsAdd() {
        return PREFIX + "tGoods_add.html";
    }

    /**
     * 跳转到修改商品
     */
    @RequestMapping("/tGoods_update/{tGoodsId}")
    public String tGoodsUpdate(@PathVariable Integer tGoodsId, Model model) {
        TGoods tGoods = TGoodsService.selectById(tGoodsId);
        model.addAttribute("tGoods",tGoods);
        return PREFIX + "tGoods_edit.html";
    }



    /**
     * 获取商品列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
          List<Map<String, Object>> list = TGoodsService.selectMaps(null);
         return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 新增商品
     */
    @RequestMapping(value = "/add")
    @ResponseBody
    public Object add(TGoods tGoods) {
        TGoodsService.insert(tGoods);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除商品
     */
    @RequestMapping(value = "/delete")
    @ResponseBody
    public Object delete(@RequestParam Long id) {
        TGoodsService.deleteById(id);
        return SUCCESS_TIP;
    }

    /**
     * 修改商品
     */
    @RequestMapping(value = "/update")
    @ResponseBody
    public Object update(TGoods tGoods) {
        TGoodsService.updateById(tGoods);
        return super.SUCCESS_TIP;
    }

}
