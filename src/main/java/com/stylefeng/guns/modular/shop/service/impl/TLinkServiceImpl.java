package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TLink;
import com.stylefeng.guns.persistence.shop.dao.TLinkMapper;
import com.stylefeng.guns.modular.shop.service.ITLinkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TLinkServiceImpl extends ServiceImpl<TLinkMapper, TLink> implements ITLinkService {
	
}
