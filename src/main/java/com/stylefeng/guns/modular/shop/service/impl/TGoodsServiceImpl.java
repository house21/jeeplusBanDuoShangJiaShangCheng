package com.stylefeng.guns.modular.shop.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.persistence.shop.model.TGoods;
import com.stylefeng.guns.persistence.shop.dao.TGoodsMapper;
import com.stylefeng.guns.modular.shop.service.ITGoodsService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.persistence.shop.model.TGoodsClass;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TGoodsServiceImpl extends ServiceImpl<TGoodsMapper, TGoods> implements ITGoodsService {
    @Resource
    private TGoodsMapper tGoodsMapper;
    @Override
    public List<TGoods> selectProductByFloor(Long id) {
        return tGoodsMapper.selectProductByFloor(id);
    }

    @Override
    public List<TGoods> getProductByFloorid(Long tid) {
        return tGoodsMapper.getProductByFloorid(tid);
    }

    @Override
    public List<TGoods> getGoodsByBrandid(Long bid) {
        TGoods goods= new TGoods();
        goods.setBrandid(bid);
        return tGoodsMapper.selectList(new EntityWrapper<>(goods));
    }

    @Override
    public Page<TGoods> selectgoodsListByType(int i, int i1, TGoods g) {
        Page<TGoods> page = new Page<TGoods>(i, i1);
        List<TGoods> list = tGoodsMapper.selectList(new EntityWrapper<>(g));
        page.setRecords(list);
        return page;
    }
}
