package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.common.node.ZTreeNode;
import com.stylefeng.guns.persistence.shop.model.TGoodsClass;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 商品分类表 服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITGoodsClassService extends IService<TGoodsClass> {

    List<ZTreeNode> tree();
}
