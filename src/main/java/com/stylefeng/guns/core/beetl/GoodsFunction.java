package com.stylefeng.guns.core.beetl;

import java.util.List;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.stylefeng.guns.core.util.SpringContextHolder;
import com.stylefeng.guns.modular.shop.service.*;
import com.stylefeng.web.utils.MemberUtils;
import com.stylefeng.guns.persistence.shop.model.*;
import org.springframework.stereotype.Component;

@Component
public class GoodsFunction {

	private ITOrderService orderService = SpringContextHolder.getBean(ITOrderService.class);
	private ITGoodsClassService itGoodsClassService = SpringContextHolder.getBean(ITGoodsClassService.class);
	private ITGoodsTypeService itGoodsTypeService = SpringContextHolder.getBean(ITGoodsTypeService.class);
	private ITCartService CartService = SpringContextHolder.getBean(ITCartService.class);
	private ITGoodsService productService = SpringContextHolder.getBean(ITGoodsService.class);
	private ITArticleService articleService = SpringContextHolder.getBean(ITArticleService.class);
	private ITStoreService itStoreService = SpringContextHolder.getBean(ITStoreService.class);
	private ITFloorService itFloorService = SpringContextHolder.getBean(ITFloorService.class);
	private ITBrandService itBrandService = SpringContextHolder.getBean(ITBrandService.class);

	public Page<TBrand> getBrands(int pageSize){
		TBrand t =new TBrand();
		t.setStat(0);
		Page<TBrand> page = new Page<TBrand>(1, pageSize);
		EntityWrapper<TBrand> ew = new EntityWrapper<>(t);
		return itBrandService.selectPage(page,ew);
	}
	public Page<TFloor> getFloors(int pageSize){
		Page<TFloor> page = new Page<TFloor>(1, pageSize);
		EntityWrapper<TFloor> ew = new EntityWrapper<>(new TFloor());
		return itFloorService.selectPage(page,ew);
	}

	public Page<TStore> getStore(int pageSize){
		TStore store = new TStore();
		store.setStoreStatus(3);
		store.setDeleteStatus(false);
		Page<TStore> page = new Page<TStore>(1, pageSize);
		EntityWrapper<TStore> ew = new EntityWrapper<>(store);
		return itStoreService.selectPage(page,ew);
	}

	public Page<TGoods> getLatestGoods(int pageSize){
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(new TGoods());
		ew.orderBy("create_date",false);
		return productService.selectPage(page,ew);
	}
	public Page<TGoods> getHitGoods(int pageSize){
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(new TGoods());
		ew.orderBy("clickHit",false);
		return productService.selectPage(page,ew);
	}
	public Page<TGoods> getReplyGoods(int pageSize){
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(new TGoods());
		ew.orderBy("replyhit",false);
		return productService.selectPage(page,ew);
	}
	/**
	 * 买的好的
	 * @param pageSize
	 * @return
	 */
	public Page<TGoods> getSellGoods(int pageSize){
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(new TGoods());
		ew.orderBy("sellhit",false);
		return productService.selectPage(page,ew);
	}


	/**
	 * 拿到推荐
	 * @param pageSize
	 * @return
	 */
	public Page<TGoods> getCommendGoods(int pageSize){
		TGoods p=new TGoods();
		p.setIscom(1);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		return productService.selectPage(page,ew);
	}
	/**
	 * 店内买的好的
	 * @param pageSize
	 * @param storeId
	 * @return
	 */
	public Page<TGoods> getStoreSellGoods(int pageSize,Long storeId){
		TGoods p=new TGoods();
		p.setStoreid(storeId);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		ew.orderBy("sellhit",false);
		return productService.selectPage(page,ew);
	}
	public Page<TGoods> getStoreLatestGoods(int pageSize,Long storeId){
		TGoods p=new TGoods();
		p.setStoreid(storeId);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		ew.orderBy("create_date",false);
		return productService.selectPage(page,ew);
	}
	public Page<TGoods> getStoreHitGoods(int pageSize,Long storeId){
		TGoods p=new TGoods();
		p.setStoreid(storeId);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		ew.orderBy("clickHit",false);
		return productService.selectPage(page,ew);
	}
	/**
	 * 拿到店内推荐
	 * @param pageSize
	 * @return
	 */
	public Page<TGoods> getStoreCommendGoods(int pageSize,Long storeId){
		TGoods p=new TGoods();
		p.setIscom(1);
		p.setStoreid(storeId);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		return productService.selectPage(page,ew);
	}

	/**
	 * 按类别
	 * @param pageSize
	 * @param typeid
	 * @return
	 */
	public Page<TGoods> getTypeGoods(int pageSize,Long typeid){
		TGoods p=new TGoods();
		p.setTypeid(typeid);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);

		return productService.selectPage(page,ew);
	}
	public Page<TGoods> getMenuTypeGoods(int pageSize,Long typeid){
		TGoods p=new TGoods();
		p.setClassid(typeid);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		return productService.selectPage(page,ew);
	}
	public Page<TGoods> getBrandGoods(int pageSize,Long brandid){
		TGoods p=new TGoods();
		p.setBrandid(brandid);
		Page<TGoods> page = new Page<TGoods>(1, pageSize);
		EntityWrapper<TGoods> ew = new EntityWrapper<>(p);
		return productService.selectPage(page,ew);
	}
	//活动最新订单
	public Page<TOrder> getLatestOrder(int pageSize){
		Page<TOrder> page = new Page<TOrder>(1, pageSize);
		EntityWrapper<TOrder> ew = new EntityWrapper<>(new TOrder());
		ew.orderBy("sellhit",false);
		return orderService.selectPage(page,ew);

	}

	public Page<TGoodsType> getProductType(int pageSize){
		TGoodsType gc=new TGoodsType();
		Page<TGoodsType> page = new Page<TGoodsType>(1, pageSize);
		EntityWrapper<TGoodsType> ew = new EntityWrapper<>(new TGoodsType());
		return itGoodsTypeService.selectPage(page,ew);
	}
	public Page<TGoodsClass> getProductClass(int pageSize){
		TGoodsClass gc=new TGoodsClass();

		Page<TGoodsClass> page = new Page<TGoodsClass>(1, pageSize);
		EntityWrapper<TGoodsClass> ew = new EntityWrapper<>(new TGoodsClass());
		return itGoodsClassService.selectPage(page,ew);
	}
	//得到菜单类别
	public Page<TGoodsClass> getProductClass(int pageSize, Long parentId){
		TGoodsClass gc=new TGoodsClass();
         gc.setPid(parentId);
		Page<TGoodsClass> page = new Page<TGoodsClass>(1, pageSize);
		EntityWrapper<TGoodsClass> ew = new EntityWrapper<>(gc);
		return itGoodsClassService.selectPage(page,ew);
	}
	public Page<TGoodsClass> getProductClassListByPid(int pageSize,Long pid){
		TGoodsClass gc=new TGoodsClass();
		gc.setPid(pid);
		Page<TGoodsClass> page = new Page<TGoodsClass>(1, pageSize);
		EntityWrapper<TGoodsClass> ew = new EntityWrapper<>(gc);
		return itGoodsClassService.selectPage(page,ew);
	}

	public List<TGoodsClass> getAllProductClass(){
		return itGoodsClassService.selectList(null);
	}
	
	//得到购物车
//	public List<Cart> getCartList() {
//		 return CartService.selectOwnCart();
//	 }
	
	//活动最新订单
		public Page<TArticle> getAdvArticle(int pageSize){
			Page<TArticle> page = new Page<TArticle>(1, pageSize);
			EntityWrapper<TArticle> ew = new EntityWrapper<>(new TArticle());
			return articleService.selectPage(page,ew);
		}


	public TMember getLoginUser(){
		return MemberUtils.getSessionLoginUser();
	}
	public boolean isLogin(){
		return MemberUtils.getSessionLoginUser()!=null;
	}
	public String subStringTo(String str){
		if(str != null && str.length() > 3){
			return "****"+str.substring(3, str.length());
		}
		return str;
	}
}
