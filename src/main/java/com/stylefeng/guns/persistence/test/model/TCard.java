package com.stylefeng.guns.persistence.test.model;

import com.baomidou.mybatisplus.enums.IdType;
import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zscat
 * @since 2017-05-22
 */
@TableName("t_card")
public class TCard extends Model<TCard> {
	private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
	private Date addTime;
	private Boolean deleteStatus;

	private String storeAddress;

	private Integer storeCredit;

	private String storeInfo;

	private String storeMsn;

	private String storeName;

	private String storeOwer;

	private String storeQq;

	private Boolean storeRecommend;

	private String storeSeoDescription;

	private String storeSeoKeywords;

	private Integer storeStatus;

	private String storeTelephone;

	private String storeZip;
	private String template;

	private Long areaId;

	private Long storeBannerId;

	private Long storeLicenseId;
	private String img;

	private Integer favoriteCount;

	private BigDecimal storeLat;

	private BigDecimal storeLng;

	private String storeWw;

	private String mapType;

	private Long storeDistribution;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public Boolean isDeleteStatus() {
		return deleteStatus;
	}

	public void setDeleteStatus(Boolean deleteStatus) {
		this.deleteStatus = deleteStatus;
	}

	public String getStoreAddress() {
		return storeAddress;
	}

	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}

	public Integer getStoreCredit() {
		return storeCredit;
	}

	public void setStoreCredit(Integer storeCredit) {
		this.storeCredit = storeCredit;
	}

	public String getStoreInfo() {
		return storeInfo;
	}

	public void setStoreInfo(String storeInfo) {
		this.storeInfo = storeInfo;
	}

	public String getStoreMsn() {
		return storeMsn;
	}

	public void setStoreMsn(String storeMsn) {
		this.storeMsn = storeMsn;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreOwer() {
		return storeOwer;
	}

	public void setStoreOwer(String storeOwer) {
		this.storeOwer = storeOwer;
	}

	public String getStoreQq() {
		return storeQq;
	}

	public void setStoreQq(String storeQq) {
		this.storeQq = storeQq;
	}

	public Boolean isStoreRecommend() {
		return storeRecommend;
	}

	public void setStoreRecommend(Boolean storeRecommend) {
		this.storeRecommend = storeRecommend;
	}

	public String getStoreSeoDescription() {
		return storeSeoDescription;
	}

	public void setStoreSeoDescription(String storeSeoDescription) {
		this.storeSeoDescription = storeSeoDescription;
	}

	public String getStoreSeoKeywords() {
		return storeSeoKeywords;
	}

	public void setStoreSeoKeywords(String storeSeoKeywords) {
		this.storeSeoKeywords = storeSeoKeywords;
	}

	public Integer getStoreStatus() {
		return storeStatus;
	}

	public void setStoreStatus(Integer storeStatus) {
		this.storeStatus = storeStatus;
	}

	public String getStoreTelephone() {
		return storeTelephone;
	}

	public void setStoreTelephone(String storeTelephone) {
		this.storeTelephone = storeTelephone;
	}

	public String getStoreZip() {
		return storeZip;
	}

	public void setStoreZip(String storeZip) {
		this.storeZip = storeZip;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public Long getStoreBannerId() {
		return storeBannerId;
	}

	public void setStoreBannerId(Long storeBannerId) {
		this.storeBannerId = storeBannerId;
	}

	public Long getStoreLicenseId() {
		return storeLicenseId;
	}

	public void setStoreLicenseId(Long storeLicenseId) {
		this.storeLicenseId = storeLicenseId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(Integer favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public BigDecimal getStoreLat() {
		return storeLat;
	}

	public void setStoreLat(BigDecimal storeLat) {
		this.storeLat = storeLat;
	}

	public BigDecimal getStoreLng() {
		return storeLng;
	}

	public void setStoreLng(BigDecimal storeLng) {
		this.storeLng = storeLng;
	}

	public String getStoreWw() {
		return storeWw;
	}

	public void setStoreWw(String storeWw) {
		this.storeWw = storeWw;
	}

	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	public Long getStoreDistribution() {
		return storeDistribution;
	}

	public void setStoreDistribution(Long storeDistribution) {
		this.storeDistribution = storeDistribution;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}
}
